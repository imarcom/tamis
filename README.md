Command to recompile scripts : ./node_modules/.bin/babel src --out-dir dist

npm install git+ssh://git@bitbucket.org/imarcom/tamis.git --save-dev

JS

    import 'tamis/dist/tamis'
    
    $(function () {
      $(document).ready(function () {
    
        let tamis = $('your-filters-container').tamis({
          $elements: $('your-element-selector'),
          fields: {
            'filter-name-a': {
              filterType: 'options'
            },
            'filter-name-b': {
              filterType: 'options'
            }
          },
          searchPlaceholder: translation('placeholder_text'),
          language: locale
        })
    
      })
    })

CSS

    @import "~tamis/tamis.scss";
    
    your-element-selector {
        display: none;
        
        &.filtered {
            display: block;
        }
    }

HTML

    <div class="tamis-selectedFilters"></div>
     
    <div class="tamis-nbResults tamis-nbResults-hidden">
        <span class="tamis-nbResults-nb"></span> result(s)
    </div>
    
    <a class="tamis-input" href="javascript:;" data-filter-type="type1" data-filter-value="value1">Value1</a>
    <a class="tamis-input" href="javascript:;" data-filter-type="type1" data-filter-value="value2">Value2</a>
    
    <a class="tamis-reset" href="javascript:;">Reset</a>
    
    <div class="filtered" data-type1="value1">Value1</div>
    <div class="filtered" data-type1="value2">Value2</div>