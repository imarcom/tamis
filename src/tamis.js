
require('./imarcomSearch.js')
var URLON = require('urlon');

$.fn.tamis = function (customOptions) {
  var $filters = $(this)

  var defaultOptions = {
    getFilters: getFilters,
    inputClass: 'tamis-input',
    inputSelectedClass: 'tamis-input-selected',
    inputDisabledClass: 'tamis-input-disabled',
    nbResultsClass: 'tamis-nbResults',
    nbResultsHiddenClass: 'tamis-nbResults-hidden',
    selectedFiltersClass: 'tamis-selectedFilters',
    selectedFilterClass: 'tamis-selectedFilter',
    resetClass: 'tamis-reset',
    resetVisibleClass: 'tamis-reset-visible',
    searchClass: 'tamis-search',
    filteredClass: 'filtered',
    splitCharacter: ' ',
    alwaysShowNbResults: true,
    restrictFilters: function (info, fromInit) {
      $filters.find('.' + (options.inputDisabledClass)).removeClass(options.inputDisabledClass)

      if (!fromInit) {
        $filters.find('.'+options.inputClass).each(function () {
          var $input = $(this)
          var filterType = $(this).data('filter-type')
          var filterValue = $(this).data('filter-value')

          if (info[filterType][filterValue] == 0) {
            $input.addClass(options.inputDisabledClass)
          }
        })
      }
    }
  }

  var options = $.extend(defaultOptions, customOptions)

  function setFilters () {
    $filters.imarcomSearch(options)
    $filters.imarcomSearch('filter')
    updateInterface()
  }
  
  function updateInterface () {
    var $nbResults = $('.'+options.nbResultsClass)
    
    // Toggle nbResult visibility
    if (!options.alwaysShowNbResults) {
      if ($filters.find('.'+options.inputSelectedClass).length) {
        $nbResults.removeClass(options.nbResultsHiddenClass)
      } else {
        $nbResults.addClass(options.nbResultsHiddenClass)
      }
    }
    
    let nbResults = options.$elements.filter('.' + options.filteredClass).length
    let nbResultsText = nbResults
    
    if (nbResults === 1) {
      nbResultsText += ' ' + $nbResults.data('singular')
    } else {
      nbResultsText += ' ' + $nbResults.data('plural')
    }
    
    // Update nbResults text
    $nbResults.text(nbResultsText)
    
    // Toggle reset button visibility
    if ($filters.find('.'+options.inputSelectedClass).length || $filters.find('.'+options.searchClass).val() != '') {
      $filters.find('.'+options.resetClass).addClass(options.resetVisibleClass)
    } else {
      $filters.find('.'+options.resetClass).removeClass(options.resetVisibleClass)
    }
  }

  function updateFilters () {
    $filters.imarcomSearch('destroy')
    setFilters()
    updateUrl()
  }

  function updateUrl () {
    var API = $filters.data('api')
    let _filters = API.getFilters()

    if (!$.isEmptyObject(_filters)) {
      history.replaceState(undefined, undefined, '#' + URLON.stringify(_filters))
      // window.location.replace = URLON.stringify(_filters);
    } else {
      history.replaceState(undefined, undefined, '#')
      // window.location.replace = "";
    }
  }

  function getFilters () {
    var selectedFilters = {}

    $filters.find('.'+options.inputClass).each(function () {
      if ($(this).hasClass(options.inputSelectedClass)) {
        var filterType = $(this).data('filter-type')
        var filterValue = $(this).data('filter-value')

        if (!selectedFilters.hasOwnProperty(filterType)) {
          selectedFilters[filterType] = []
        }

        selectedFilters[filterType].push(filterValue)
      }
    })
    
    $filters.find('.'+options.searchClass).each(function () {
        let searchValue = $(this).val()
        if (searchValue !== '') {
            selectedFilters[$(this).attr('name')] = searchValue
        }
    })
    
    return selectedFilters
  }

  $filters.find('.'+options.inputClass).on('click', function () {
    if (!$(this).hasClass(options.inputDisabledClass)) {
      $(this).toggleClass(options.inputSelectedClass)
      updateFilters()

      var filterType = $(this).data('filter-type')
      var filterValue = $(this).data('filter-value')

      if ($(this).hasClass(options.inputSelectedClass)) {
        if ($('.'+options.selectedFiltersClass).length) {
          var filterName = $(this).text()

          $('.'+options.selectedFiltersClass).append('<a class="'+options.selectedFilterClass+'" href="javascript:;" data-filter-type="' + filterType + '" data-filter-value="' + filterValue + '">' + filterName + '&nbsp;&nbsp;&nbsp;X</a>')
        }
      } else {
        var $selectedFilter = $filters.find('.'+options.selectedFilterClass+'[data-filter-type="' + filterType + '"][data-filter-value="' + filterValue + '"]')

        if ($selectedFilter.length) {
          $selectedFilter.remove()
        }
      }
    }
  })

  $filters.find('.'+options.resetClass).on('click', function () {
    $filters.find('.'+options.inputSelectedClass).removeClass(options.inputSelectedClass)
    $('.'+options.selectedFiltersClass).html('')
    $filters.find('.'+options.searchClass).val('').trigger('change')
    updateFilters()
  })

  $filters.find('.'+options.selectedFiltersClass).on('click', '.'+options.selectedFilterClass, function () {
    var filterType = $(this).data('filter-type')
    var filterValue = $(this).data('filter-value')

    $filters.find('.'+options.inputClass+'[data-filter-type="' + filterType + '"][data-filter-value="' + filterValue + '"]').trigger('click')
  })
  
  $filters.find('.'+options.searchClass).on('change', () => {
    $filters.imarcomSearch('filter')
    updateInterface()
    updateUrl()
  })

  setFilters()

  var hash = window.location.hash.replace(/^#/, '')

  if (hash != '' && _.includes(hash, '$') && _.includes(hash, '@')) {
    var optionsFromUrl = URLON.parse(hash)

    $.each(optionsFromUrl, function (filterType, values) {
      if (typeof values === 'string') {
        values = [values]
      }
      $.map(values, function (filterValue) {
        $filters.find('.'+options.inputClass+'[data-filter-type="' + filterType + '"][data-filter-value="' + filterValue + '"]').trigger('click')
        $filters.find('.'+options.searchClass+'[name="' + filterType + '"]').val(filterValue).trigger('change')
      })
    })
  }
}
