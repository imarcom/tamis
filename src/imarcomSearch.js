(function ($) {
  /**
    * To initialize
    * $('#search').imarcomSearch({
    *       $elements: $('#container').children(),
    *       fields: {...},
    *       ...
    * })
    *
    * To trigger a filter
    * $('#search').imarcomSearch('filter')
    *
    * To trigger a sort
    * $('#search').imarcomSearch('sort', ['fieldname', +-1])
    *
    * Disable/Enable filtering, usefull for batch modification of fields like reset
    * $('#search').imarcomSearch('filter-disable');
    * $('#search').imarcomSearch('filter-enable');
    */
  require('select2/dist/js/select2.full.min')
  require('select2/dist/js/i18n/fr')

  $.fn.imarcomSearch = function (optionsOrCommand, commandParams) {
    if (typeof optionsOrCommand === 'string') {
      return this.trigger(optionsOrCommand + '.imc-js-search', commandParams)
    }

    var settings = $.extend({
      /**
            * List of filterable and sortable elements
            */
      $elements: $([]),

      /**
            * List of fields
            * {
            *   'category': {
            *       filterType: 'options',
            *   },
            *   'price': {
            *       filterType: 'range',
            *       sortable:true
            *   },
            *   'search': {
            *       filterType: 'text'
            *   },
            *   'new': {
            *       sortable:true
            *   }
            *}
            */
      fields: [],

      /**
            * This function should to hide or disable options that will lead to no results.
            * It takes one parameter, an object decribing the possible outcomes. For example :
            * {
            *   'category': {
            *       'cat1': false, // no results
            *       'cat2': true, // results
            *   },
            *   'price': {
            *       min:0,
            *       max:100,
            *   }
            * }
            */
      restrictFilters: false,

      /**
            * This function should return the current filters selected. For example,
            * {
            *   'category': ['cat1']
            *   'price': {
            *       min:0,
            *       max:100,
            *   }
            * }
            */
      getFilters: function () {},

      /**
            * This function is called after filter function,
            */
      afterFilter: function (settings) {},
      sort: function sort (key, direction) {
        function compare (a, b) {
          if (a[key] < b[key]) {
            return 1 * direction
          }
          if (a[key] > b[key]) {
            return -1 * direction
          }
          if (a.index < b.index) {
            return 1
          }
          if (a.index > b.index) {
            return -1
          }
          return 0
        }
        sortArray.sort(compare)
        $container.detach()
        for (var i = 0; i < sortArray.length; i++) {
          $container.append(sortArray[i].element)
        }
        $containerParent.append($container)
      },

      /**
            * This function is called after sort function,
            */
      afterSort: function (settings) {},
      searchPlaceholder: 'Search',
      language: 'en',
    }, optionsOrCommand)

    var sortableFields = []
    var filterableFields = []
    var $container = settings.$elements.eq(0).parent() // Elements parent node used to sort
    var $containerParent = $container.parent() // Used to detach and reattach container
    var sortArray = [] // Used to sort efficiently
    var filterEnabled = true
    var that = this

    var keywords = ''

    /**
        * Execute once at startup
        */
    function init () {
      // Build sortableFields and filterableFields, handles default values on fields.
      var fieldname, config, fieldList = []

      for (fieldname in settings.fields) {
        fieldList.push(fieldname)
        config = settings.fields[fieldname] // pointer

        // sortable or not
        if (typeof config.sortable !== 'undefined' && config.sortable) {
          sortableFields.push(fieldname)
        } else {
          settings.fields[fieldname].sortable = false
        }

        // filter type
        if (typeof config.filterType !== 'undefined' && config.filterType) {
          filterableFields.push(fieldname)
          if (config.filterType == 'options' || config.filterType == 'keyword') {
            settings.fields[fieldname].values = {}
          }
        } else {
          settings.fields[fieldname].filterType = false
        }
      };

      fieldList = $.unique(fieldList)

      // Calculate the unique values of options filters and the base min and max of range filters
      // Build sortArray to sort efficiently
      var uniqueIds = {}

      settings.$elements.each(function (index) {
        var $this = $(this),
          id = $this.attr('id'),
          field,
          value,
          element = {
            element: this,
            index: index
          }

        var keywordAttribute = $this.data('search')
        if (keywordAttribute != undefined) {
          if (keywords !== '') {
            keywords = keywords + settings.splitCharacter + keywordAttribute
          } else {
            keywords = keywordAttribute
          }
        }

        $.each(fieldList, function () {
          field = this.toString()
          value = $this.data(field)

          if (typeof uniqueIds[field] === 'undefined') {
            uniqueIds[field] = {}
          }
          if (uniqueIds[field][id] === true) return

          if (settings.fields[field].sortable) {
            element[field] = value
          }

          if (settings.fields[field].filterType == 'range') {
            if (typeof settings.fields[field].min === 'undefined') {
              settings.fields[field].min = value
              settings.fields[field].max = value
            } else {
              if (value < settings.fields[field].min) {
                settings.fields[field].min = value
              }
              if (value > settings.fields[field].max) {
                settings.fields[field].max = value
              }
            }
          } else if (settings.fields[field].filterType == 'text') {
            settings.fields[field].value = value
          } else if (settings.fields[field].filterType == 'options' || settings.fields[field].filterType == 'keyword') {
            if (typeof value === 'string' && value.length > 0) {
              var values = value.split(settings.splitCharacter)

              for (var j = 0; j < values.length; j++) {
                // use object key to prevent too many results
                // assuming that an item may apear more than once
                if (typeof settings.fields[field].values[values[j]] === 'undefined') {
                  settings.fields[field].values[values[j]] = {}
                }
                settings.fields[field].values[values[j]].id = true

                $this.addClass(field + '-' + _toUrl(values[j]))
              }
            } else {
              // use object key to prevent too many results
              // assuming that an item may apear more than once
              if (typeof settings.fields[field].values[value] === 'undefined') {
                settings.fields[field].values[value] = {}
              }
              settings.fields[field].values[value][id] = true

              $this.addClass(field + '-' + value)
            }
          }
        })

        sortArray.push(element)
      })

      // build keyword list
      keywords = keywords.split(settings.splitCharacter)
      var filteredKeywords = []
      var knownKeys = {}
      for (var i = 0; i < keywords.length; i++) {
        if (keywords[i] != '') {
          if (knownKeys[keywords[i]] !== true) {
            knownKeys[keywords[i]] = true
            filteredKeywords.push({
              'id': _toUrl(keywords[i]),
              'text': keywords[i].replace(/\_/g, ' ', keywords[i]).trim()
            })
          }
        }
      }

      // convert array key to count
      $.each(settings.fields, function () {
        var that = this
        if (this.filterType != 'keyword') {
          $.each(this.values, function (key) {
            that.values[key] = objSize(this)
          })
        } else {
          if (filteredKeywords.length) {
            var $select2 = $('.'+settings.searchClass)

            $select2.select2({
              data: filteredKeywords,
              placeholder: settings.searchPlaceholder,
              language: settings.language,
              allowClear: true,
            })

            if ($select2.val() !== '') {
              $select2.trigger('change') // triggering change events
            }
          }
        }
      })
    }

    function _toUrl (value) {
      value = value.toLowerCase()

      value = value.replace(/[àâ]/g, 'a')
      value = value.replace(/[éèê]/g, 'e')
      value = value.replace(/[ï]/g, 'i')
      value = value.replace(/[ô]/g, 'o')
      value = value.replace(/[û]/g, 'u')

      value = value.replace(/[^a-z0-9]/g, '-')
      value = value.replace(/[\_]+/, '-')

      return value
    }

    function objSize (obj) {
      var count = 0

      if (typeof obj === 'object') {
        if (Object.keys) {
          count = Object.keys(obj).length
        } else if (window._) {
          count = _.keys(obj).length
        } else if (window.$) {
          count = $.map(obj, function () { return 1 }).length
        } else {
          for (var key in obj) if (obj.hasOwnProperty(key)) count++
        }
      }

      return count
    }
    /**
        * Sorts the $elements
        */
    function sort (key, direction) {
      settings.sort.call(that, key, direction)

      if (typeof settings.afterSort === 'function') {
        settings.afterSort.call(that, settings)
      }
    }

    /**
        * Returns the results set of a filter
        */
    function getResults (selectedFilters) {
      var $results = settings.$elements, field

      for (field in selectedFilters) {
        if (settings.fields[field].filterType == 'options' || settings.fields[field].filterType == 'keyword') {
          var selectors = []

          var values = selectedFilters[field]

          if (typeof values === 'string') {
            values = [values]
          }

          for (var i = 0; i < values.length; i++) {
            selectors.push('.' + field + '-' + values[i])
          }

          $results = $results.filter(selectors.join(','))
        } else if (settings.fields[field].filterType == 'range') {
          $results = $results.filter(function (index) {
            var value = $(this).data(field)
            return (
              (typeof selectedFilters[field].min === 'undefined' || value >= selectedFilters[field].min) &&
                            (typeof selectedFilters[field].max === 'undefined' || value <= selectedFilters[field].max)
            )
          })
        } else if (settings.fields[field].filterType == 'text') {
          $results = $results.filter(function () {
            return selectedFilters[field].indexOf($(this).getAttr('id')) !== -1
          })
        }
      }
      return $results
    }

    /**
        * Filters the $elements
        */
    function filter () {
      var selectedFilters = settings.getFilters()
      var $result = getResults(selectedFilters)
      settings.$elements.removeClass(settings.filteredClass)
      $result.addClass(settings.filteredClass)

      if (typeof settings.restrictFilters === 'function') {
        settings.restrictFilters(computeFiltersStatus(selectedFilters, $result))
      }

      if (typeof settings.afterFilter === 'function') {
        settings.afterFilter.call(that, settings)
      }
    }

    /**
        * Calculate which values returns no results for all fields
        */
    function computeFiltersStatus (selectedFilters, $result) {
      var filtersStatus = {}

      for (var i = 0; i < filterableFields.length; i++) {
        var field = filterableFields[i]
        if (typeof selectedFilters[field] === 'undefined') {
          // No current filters for this column, use result set
          filtersStatus[field] = getPossibleOptions(field, $result)
        } else {
          // There's a current filter for this column, exclude it
          var testFilters = jQuery.extend(true, {}, selectedFilters)
          delete testFilters[field]
          filtersStatus[field] = getPossibleOptions(field, getResults(testFilters))
        }
      }

      return filtersStatus
    }

    /**
        * Calculate which values returns no results for a specific field
        */
    function getPossibleOptions (field, $resultSet) {
      if (settings.fields[field].filterType == 'options') {
        if ($resultSet.length == settings.$elements.length) {
          // Full set, return all possible values
          return settings.fields[field].values
        }
        // Calculating possible values
        var possibleValues = {}, foundElements
        for (var value in settings.fields[field].values) {
          // calculate count of unique items - assuming that an item may apear more than once
          foundElements = []
          $resultSet.filter('.' + field + '-' + value).each(function (index) {
            foundElements.push($(this).attr('id') ? $(this).attr('id') : index)
          })
          foundElements = $.unique(foundElements)

          possibleValues[value] = $.unique(foundElements).length

          // possibleValues[value] = $resultSet.filter('.'+field+'-'+value).length;
        }
        return possibleValues
      } else if (settings.fields[field].filterType == 'range') {
        if ($resultSet.length == settings.$elements.length) {
          // Full set, return all possible values
          return {
            min: settings.fields[field].min,
            max: settings.fields[field].max
          }
        }
        var min = settings.fields[field].max
        var max = settings.fields[field].min
        $resultSet.each(function () {
          var value = $(this).data(field)
          if (value < min) {
            min = value
          }
          if (value > max) {
            max = value
          }
        })
        return {
          min: min,
          max: max
        }
      } else if (settings.fields[field].filterType == 'text') {
        return ''
      }
    }

    /**
        * Startup
        */
    init()
    if (typeof settings.restrictFilters === 'function') {
      settings.restrictFilters(computeFiltersStatus({}, settings.$elements), true)
    }

    /**
        * Binding event to be able to trigger filter and sort.
        */
    this
      .bind('filter.imc-js-search', function () {
        if (filterEnabled) {
          filter()
        }
      })
      .bind('sort.imc-js-search', function (e, field, direction) {
        sort(field, direction)
      })
      .bind('filter-enable.imc-js-search', function (e) {
        filterEnabled = true
      })
      .bind('filter-disable.imc-js-search', function (e) {
        filterEnabled = false
      })
      .bind('destroy.imc-js-search', function (e) {
        settings.$elements.removeClass((settings.filteredClass) + ' hidden')
        settings = {}
        $(this).unbind('.imc-js-search')
      })

    this.data('api', {
      getFilters: settings.getFilters,
      getFields: function () {
        return settings.fields
      }
    })

    return this
  }
})(jQuery)
